from typing import final
from unittest import result
import speech_recognition as sr
import spacy
from flask import logging, Flask, render_template, request, flash
import pandas as pd


app = Flask(__name__)
app.secret_key = "SanaAmmar"

@app.route('/')
def index():
    flash(" Welcome to SanaAmmar's travel site")
    return render_template('index.html')

@app.route('/audio_to_text/')
def audio_to_text():
    flash(" Press Start to start recording audio and press Stop to end recording audio")
    return render_template('audio_to_text.html')

@app.route('/audio', methods=['POST'])
def audio():
    r = sr.Recognizer()
    nlp = spacy.load("fr_core_news_sm")
    with open('upload/audio.wav', 'wb') as f:
        f.write(request.data)
  
    with sr.AudioFile('upload/audio.wav') as source:
        audio_data = r.record(source)
        text = r.recognize_google(audio_data, language='fr-FR', show_all=True)
        print(text)
        # return_text = " Vous avez dit: <br> "
        return_text=""
        try:
            return_text += text['alternative'][0]['transcript']
        except:
            return_text = " Erreur de detection"
        
    #Partie Benjamin 
    audio_text = nlp(return_text)

    # show entities
    finalText="Depart "
    departure=""
    destination=""
    indd=0
    for entity in audio_text.ents:
        print(entity.label_,' : ',entity.text)

        if(entity.label_=="LOC"):  
            indd+=1
            if(indd==1):
                departure=entity.text
            elif(indd>1):
                destination=entity.text
 
    finalText=('Depart de : ',departure, ' à ',destination,' index',indd)
    #print(finalText)
    finalText=getTrajet(extractData(),departure,destination)

        
    return str(finalText)

def extractData():
    df = pd.read_csv('dataset/timetables.csv')

    columns_of_interest = ['TripId', 'Departure', 'Destination', 'Duration']
    data = df[columns_of_interest]


    dataController = []

    i = 0
    while i < len(data):
        trips = {
            "id": data.TripId[i],
            "departure": data.Departure[i], 
            "destination": data.Destination[i],
            "duration": data.Duration[i]
        }
        dataController.append(trips)
        i += 1

    return dataController

def getTrajet(donnes,departure,destination):
    textresult=""
    travels = []

    trip = depDest(donnes, departure, destination)
    if(trip):
        travels.append(trip)

    else:
        result=("Aucun trajet direct de la " 
            + departure + " Ã  la " + destination)
        

    listAllDestinationFromDeparture = getAllDestinationFromDeparture(donnes, departure)

    i = 0
    while i < len(listAllDestinationFromDeparture):
        if(depDest(donnes, listAllDestinationFromDeparture[i]["destination"], destination)):
            trip = {
                "id": donnes[i]["id"],
                "departure": departure, 
                "destination1": listAllDestinationFromDeparture[i]["destination"],
                "destination2": destination,
                "duration": listAllDestinationFromDeparture[i]["duration"] + depDest(donnes, listAllDestinationFromDeparture[i]["destination"], destination)["duration"]
            }
            travels.append(trip)
        i += 1

    sorted_json_data = sorted(travels, key=lambda k: (int(k['duration'])), reverse=False)
    optimalTravel = sorted_json_data
    print(travels)
    print("-----------------------------------------------------------")
    print("Resultat :")
    print("Le trajet le plus court est ")
    if(len(optimalTravel)==0):
        return result
    print(optimalTravel[0])
    if(len(optimalTravel[0]['destination2'])>0):
        result=('Depart : ',optimalTravel[0]['departure'],' Passe par : ',optimalTravel[0]['destination1'],' Destination : ',optimalTravel[0]['destination2'],' Duree : ',optimalTravel[0]['duration'])
    else:
        result=('Depart : ',optimalTravel[0]['departure'],' Destination : ',optimalTravel[0]['destination1'],' Duree : ',optimalTravel[0]['duration'])

    return result;
def depDest(donnes, departure, destination):
    i = 0
    while i < len(donnes):  
        if ((departure.lower() in donnes[i]["departure"].lower()) and (destination.lower() in donnes[i]["destination"].lower())):
            duration = donnes[i]["duration"]
            trip = {
                "id": donnes[i]["id"],
                "departure": departure, 
                "destination1": destination,
                "destination2": "",
                "duration": donnes[i]["duration"]
            }
            return trip

        i += 1

def getAllDestinationFromDeparture(dataController, departure):
    listAllDestinationFromDeparture = []   
    i = 0
    while i < len(dataController):
        if (departure.lower() in dataController[i]["departure"].lower()):
            trips = {
                "id": dataController[i]["id"],
                "departure": departure, 
                "destination": dataController[i]["destination"],
                "duration": dataController[i]["duration"]
            }
            listAllDestinationFromDeparture.append(trips)
        i += 1
    
    return listAllDestinationFromDeparture
    

if __name__ == "__main__":
    app.run(debug=True)
